﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Interfaces;
using CommonInterfaces;
using DAL;
using Microsoft.Extensions.Options;

namespace Services.DataStorage
{
    /// <summary>
    /// Save data to file system and db
    /// </summary>
    public class MultipleDataStorage : IDataStorage
    {
        private readonly IDataStorage dbDataStorage;
        private readonly IDataStorage fileSystemDataStorage;
        public MultipleDataStorage(IOptions<AppSettings> options, IreckonuContext context, ILog log)
        {
            dbDataStorage = new DbDataStorage(context,log);
            fileSystemDataStorage = new FileSystemDataStore(log,options);
        }
        public async Task<bool> Save(IEnumerable<IreckonuData> datas)
        {
            var result1 = await fileSystemDataStorage.Save(datas);
            var result2 = await dbDataStorage.Save(datas);
            return result1 && result2;
        }
    }
}
