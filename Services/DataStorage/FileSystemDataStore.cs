﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Interfaces;
using CommonInterfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Services.DataStorage
{
    /// <summary>
    /// Save data to file system
    /// </summary>
    public class FileSystemDataStore : IDataStorage
    {
        private readonly ILog log;
        private readonly string fullpath;

        public FileSystemDataStore(ILog log, IOptions<AppSettings> options)
        {
            this.log = log;
            this.fullpath = options.Value?.fullpath;
        }
        public async Task<bool> Save(IEnumerable<IreckonuData> datas)
        {
            try
            {
                var jsonStr = JsonConvert.SerializeObject(datas);
                var filename = DateTimeOffset.UtcNow.ToString("yyyy_MM_dd_HH_mm_ss") + ".json";
                var fullPath = fullpath + filename;
                await File.WriteAllTextAsync(fullPath, jsonStr);
                return true;
            }
            catch (Exception ex)
            {
                log.Save(ex,"Occured error when save file to file system");
                return false;
            }
        }
    }
}
