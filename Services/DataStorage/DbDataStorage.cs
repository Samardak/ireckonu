﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.Interfaces;
using CommonInterfaces;
using DAL;
using DAL.models;
using Microsoft.EntityFrameworkCore.Internal;

namespace Services.DataStorage
{
    /// <summary>
    /// Save data to db using entity framework
    /// </summary>
    public class DbDataStorage : IDataStorage
    {
        private readonly IreckonuContext ireckonuContext;
        private readonly ILog log;
        public DbDataStorage(IreckonuContext  context, ILog log)
        {
            this.ireckonuContext = context;
            this.log = log;
        }
        public async Task<bool> Save(IEnumerable<IreckonuData> datas)
        {
            try
            {
                List<string> deliveredIn = new List<string>();
                List<string> colors = new List<string>();
                List<string> artikelCodes = new List<string>();

                foreach (var d in datas)
                {
                    deliveredIn.Add(d.DeliveredIn);
                    colors.Add(d.Color);
                    artikelCodes.Add(d.ArtikelCode);
                }

                deliveredIn = deliveredIn.Distinct().ToList();
                colors = colors.Distinct().ToList();
                artikelCodes = artikelCodes.Distinct().ToList();

                var dbDeliveredIn = ireckonuContext.DeliveredIn.ToList();
                var dbColors = ireckonuContext.Color.ToList();
                var dbArtikelCodes = ireckonuContext.Artikel.ToList();

                var dictDelivered = dbDeliveredIn.ToDictionary(i => i.DeliveredInValue);
                var dictColors = dbColors.ToDictionary(i => i.ColorName);
                var dictArtikelCodes = dbArtikelCodes.ToDictionary(i => i.ArtikelCode);

                foreach (var d in deliveredIn)
                {
                    if (dictDelivered.TryGetValue(d, out var deliveredInItem))
                        continue;
                    deliveredInItem = new DeliveredIn
                    {
                        DeliveredInValue = d
                    };
                    ireckonuContext.DeliveredIn.Add(deliveredInItem);
                    dictDelivered[d] = deliveredInItem;
                }

                foreach (var c in colors)
                {
                    if (dictColors.TryGetValue(c, out var colorItem))
                        continue;
                    colorItem = new Color { ColorName = c };
                    ireckonuContext.Color.Add(colorItem);
                    dictColors[c] = colorItem;
                }

                foreach (var a in artikelCodes)
                {
                    if (dictArtikelCodes.TryGetValue(a, out var artiketItem))
                        continue;
                    var rowWithArtikelCode = datas.FirstOrDefault(i => i.ArtikelCode == a);
                    if (rowWithArtikelCode == null)
                        continue;

                    artiketItem = new Artikel
                    {
                        ArtikelCode = rowWithArtikelCode.ArtikelCode,
                        Description = rowWithArtikelCode.Description,
                        Q1 = rowWithArtikelCode.Q1
                    };
                    ireckonuContext.Artikel.Add(artiketItem);
                    dictArtikelCodes[a] = artiketItem;
                }

                await ireckonuContext.SaveChangesAsync();

                foreach (var ireckonuData in datas)
                {
                    if (!dictArtikelCodes.TryGetValue(ireckonuData.ArtikelCode, out var artikel))
                        continue; //write warning to log file.

                    if (!dictColors.TryGetValue(ireckonuData.Color, out var color))
                        continue; //write warning to log file.

                    if (!dictDelivered.TryGetValue(ireckonuData.DeliveredIn, out var deliveredInItem))
                        continue; //write warning to log file.

                    var product = new Product
                    {
                        DeliveredInId = deliveredInItem.DeliveredInId,
                        ArtikelId = artikel.ArtikelId,
                        ColorId = color.ColorId,
                        DiscountPrice = ireckonuData.DiscountPrice,
                        Key = ireckonuData.Key,
                        Size = ireckonuData.Size,
                    };
                    ireckonuContext.Product.Add(product);
                }
                await ireckonuContext.SaveChangesAsync();

                return true;
            }
            catch (System.Exception ex)
            {
                log.Save(ex, "Occured exception when try save to db.");
                return false;
            }
        }
    }
}
