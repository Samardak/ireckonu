﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using Common.Interfaces;
using CommonInterfaces;

namespace Services
{
    /// <summary>
    /// According to benchmarks tests we use string split to parse data
    /// because it is faster then CsvHelper and TinyCsvParser nuget 
    /// </summary>
    public class DataParser : IParser
    {
        private readonly ILog log;
        public DataParser(ILog log)
        {
            this.log = log;
        }
        public List<IreckonuData> Parse(string data)
        {
            try
            {
                if (string.IsNullOrEmpty(data))
                    return null;
                List<IreckonuData> ret = new List<IreckonuData>();

                var records = data.Split(Environment.NewLine);
                var numRecords = records.Length;
                for (var recordNum = 1; recordNum < numRecords; recordNum++)
                {
                    var values = records[recordNum].Split(',');
                    var numValues = values.Length;

                    if (numValues != 10)
                        continue;
                    IreckonuData item = new IreckonuData();
                    item.Key = values[0];
                    item.ArtikelCode = values[1];
                    item.ColorCode = values[2];
                    item.Description = values[3];
                    long.TryParse(values[4], out var price);
                    item.Price = price;
                    long.TryParse(values[5], out var discountPrice);
                    item.DiscountPrice = discountPrice;
                    item.DeliveredIn = values[6];
                    item.Q1 = values[7];
                    int.TryParse(values[8], out var size);
                    item.Size = size;
                    item.Color = values[9];
                    ret.Add(item);
                }
                return ret;
            }
            catch (Exception ex)
            {
                log.Save(ex);
                return null;
            }
        }
    }
}
