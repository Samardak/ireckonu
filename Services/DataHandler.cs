﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using Common.Interfaces;

namespace Services
{
    /// <summary>
    /// Handle data. Save data to storage according type of storage in appsettings.json
    /// </summary>
    public class DataHandler : IDataHandler
    {
        private readonly IParser parser;
        private readonly IDataStorage dataStorage;
        public DataHandler(IParser parser, IDataStorage dataStorage)
        {
            this.parser = parser;
            this.dataStorage = dataStorage;
        }
        public async Task<ResponseCodeEnum> Handle(string data)
        {
            var ireckonuDatas = parser.Parse(data);
            if (ireckonuDatas == null)
                return ResponseCodeEnum.ParsedDataIsNull;

            if (!ireckonuDatas.Any())
                return ResponseCodeEnum.ParsedDataNoContainsElements;

            var saveResult = await dataStorage.Save(ireckonuDatas);
            if(!saveResult)
                return ResponseCodeEnum.DataStorageFailed;
            return ResponseCodeEnum.Ok;
        }
    }
}
