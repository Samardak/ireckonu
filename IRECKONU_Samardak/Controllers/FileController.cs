﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Enum;
using Common.Interfaces;
using Common.RequestResponse;
using CommonInterfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace WebApi.Controllers
{
    public class FileController : Controller
    {
        // by default private modifier
        readonly ILog log;
        readonly IDataHandler dataHandler;
        readonly AppSettings appSettings;
        private readonly List<string> allowedFileExtensions;
        private readonly long MaxSize;
        public FileController(
            IOptions<AppSettings> appOptions, 
            IDataHandler dataHandler,
            ILog log)
        {
            this.log = log;
            this.dataHandler = dataHandler;
            this.appSettings = appOptions.Value;
            this.MaxSize = appSettings.MaxSizeFileMB * 1024 * 1024;
            this.allowedFileExtensions = appOptions.Value?.allowedFileExtensions?.Any() ?? false ? 
                appOptions.Value?.allowedFileExtensions : new List<string> { ".csv", ".txt" };
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> Upload([Required][FromForm] IFormFile uploadedFile)
        {
            try
            {
                IFormFile postedFile = uploadedFile;
                if (postedFile == null)
                    return StatusCode((int)HttpStatusCode.BadRequest, new ResponseError(ResponseCodeEnum.PostedFileIsNull));

                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));

                //var fileName = postedFile.FileName;
                var extension = ext.ToLower();

                if (!allowedFileExtensions.Contains(extension))
                    return StatusCode((int)HttpStatusCode.BadRequest, new ResponseError(ResponseCodeEnum.IncorrectFileExtension));
                if (postedFile.Length > this.MaxSize)
                    return StatusCode((int)HttpStatusCode.BadRequest, new ResponseError(ResponseCodeEnum.ReachLimitSizeFile,
                        param: appSettings.MaxSizeFileMB));

                var allTextInFile = new StringBuilder();
                using (var reader = new StreamReader(uploadedFile.OpenReadStream()))
                {
                    while (reader.Peek() >= 0)
                        allTextInFile.AppendLine(await reader.ReadLineAsync());
                }
                var result = await dataHandler.Handle(allTextInFile.ToString());

                if(result != ResponseCodeEnum.Ok)
                    return StatusCode((int)HttpStatusCode.Conflict,
                        new ResponseError(result,
                            param: appSettings.MaxSizeFileMB));


                return StatusCode((int)HttpStatusCode.OK);
                
            }
            catch (Exception exception)
            {
                log.Save(exception);
                return StatusCode((int) HttpStatusCode.InternalServerError);
            }
        }
        
    }
}