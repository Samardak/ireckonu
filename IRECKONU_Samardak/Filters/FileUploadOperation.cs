﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace WebApi.Filters
{
    /// <summary>
    /// Now swagger can have button to upload files.ss
    /// </summary>
    public class FileUploadOperation : IOperationFilter
    {
        //http://www.talkingdotnet.com/how-to-upload-file-via-swagger-in-asp-net-core-web-api/
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.OperationId.ToLower() == "upload")
            {
                //operation.Parameters.Clear();
                operation.Parameters = new List<IParameter>();
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "uploadedFile",
                    In = "formData",
                    Description = "Upload File",
                    Required = true,
                    Type = "file"
                });
                operation.Consumes.Add("multipart/form-data");
            }
        }
    }
}
