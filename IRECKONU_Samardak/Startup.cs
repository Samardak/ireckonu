﻿using System;
using Common;
using Common.Enum;
using Common.Interfaces;
using CommonInterfaces;
using DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using WebApi.Filters;
using NLogger;
using Services;
using Services.DataStorage;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<AppSettings>(Configuration);
            


            services.AddDbContext<IreckonuContext>
                (opts => opts.UseSqlServer(Configuration["ConnectionString:DefaultConnection"]));

            services.AddSingleton<ILog, NLogger.NLogger>();
            services.AddSingleton<IParser, DataParser>();

            var storageTypeStr = Configuration["storageType"];
            int.TryParse(storageTypeStr, out var storageType);

            DataStorageType dataStorageType = (DataStorageType) storageType;
            switch (dataStorageType)
            {
                case DataStorageType.Db:
                    services.AddSingleton<IDataStorage, DbDataStorage>();
                    break;
                case DataStorageType.FileSystem:
                    services.AddSingleton<IDataStorage, FileSystemDataStore>();
                    break;
                case DataStorageType.Multiple:
                    services.AddSingleton<IDataStorage, MultipleDataStorage>();
                    break;
                default:
                    services.AddSingleton<IDataStorage, MultipleDataStorage>();
                    break;
            }

            services.AddSingleton<IDataHandler,DataHandler>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = " API",
                    Description = "IRECKONU Api for uploading file and save to db and file system.",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Nikita Samardak",
                        Email = "nvsamardak@gmail.com",
                        Url = "https://www.ireckonu.com/"
                    },
                    License = new License
                    {
                        Name = "ireckonu.com",
                        Url = "https://www.ireckonu.com/"
                    }
                });
                c.OperationFilter<FileUploadOperation>();
                //c.IncludeXmlComments("");
            });

            var serviceProvider = services.BuildServiceProvider();
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "IRECKONU Api.");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}",
                    defaults: new { controller = "File", action = "Upload" });
            });
        }
    }
}
