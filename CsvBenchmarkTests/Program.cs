﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using CommandLine;
using Common;
using CsvHelper;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.TypeConverter;

namespace CsvBenchmarkTests
{

    #region type(s)
    class CsvAutomobileMapping : CsvMapping<IreckonuData>
    {
        public CsvAutomobileMapping() : base()
        {
            MapProperty(0, x => x.Key);
            MapProperty(1, x => x.ArtikelCode);
            MapProperty(2, x => x.ColorCode);
            MapProperty(3, x => x.Description);
            MapProperty(4, x => x.Price);
            MapProperty(5, x => x.DiscountPrice);
            MapProperty(6, x => x.DeliveredIn);
            MapProperty(7, x => x.Q1);
            MapProperty(8, x => x.Size);
            MapProperty(9, x => x.Color);
        }
    } 
    #endregion

    [MemoryDiagnoser]
    public class CsvBenchmarking
    {
        public static string pathToCSV =
            @"C:\Users\nikita.samardak\source\repos\IRECKONU_Samardak\CsvBenchmarkTests\iru-assignment-2018.csv";
        [Benchmark(Baseline = true)]
        public List<IreckonuData> CSVHelper()
        {
            TextReader reader = new StreamReader(pathToCSV);
            var csvReader = new CsvReader(reader);
            var records = csvReader.GetRecords<IreckonuData>();
            return records.ToList();
        }

        [Benchmark]
        public List<IreckonuData> TinyCsvParser()
        {
            CsvParserOptions csvParserOptions = new CsvParserOptions(true, ',');
            var csvParser = new CsvParser<IreckonuData>(csvParserOptions, new CsvAutomobileMapping());

            var records = csvParser.ReadFromFile(pathToCSV, Encoding.UTF8);

            return records.Select(x => x.Result).ToList();
        }

        [Benchmark]
        public List<IreckonuData> StringSplit()
        {
            List<IreckonuData> ret = new List<IreckonuData>();

            TextReader reader = new StreamReader(pathToCSV);
            var str = reader.ReadToEndAsync().Result;
            var records = str.Split(Environment.NewLine);
            var numRecords = records.Length;
            for (var recordNum = 1; recordNum < numRecords; recordNum++)
            {
                var values = records[recordNum].Split(',');
                var numValues = values.Length;
                
                if(numValues != 10)
                    continue;
                IreckonuData item = new IreckonuData();
                item.Key = values[0];
                item.ArtikelCode = values[1];
                item.ColorCode = values[2];
                item.Description = values[3];
                long.TryParse(values[4], out var price);
                item.Price = price;
                long.TryParse(values[5], out var discountPrice);
                item.DiscountPrice = discountPrice;
                item.DeliveredIn = values[6];
                item.Q1 = values[7];
                int.TryParse(values[8], out var size);
                item.Size = size;
                item.Color = values[9];
                ret.Add(item);
            }
            return ret;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            //using (StreamWriter sw = File.AppendText(CsvBenchmarking.pathToCSV))
            //{
            //    sw.WriteLine("N00000002zwar/l30110,2,broek,Gaastra,8,0,1-3 werkdagen,baby,110,zwart");
            //}

            var summary = BenchmarkRunner.Run<CsvBenchmarking>();
            Console.ReadLine();
        }
    }
}
