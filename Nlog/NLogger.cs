﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonInterfaces;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace NLogger
{
    /// <summary>
    /// Current class imlement ILog interface and  uses NLog library.
    /// </summary>
    public class NLogger : ILog
    {
        #region Const(s)
        //Layout of date format 
        private const string _layout = @"${date:format=dd.MM.yyyy HH\:mm\:ss} ${level}: ${message} ${exception:format=toString,Data}";
        //Describes path for "info" file
        private const string _pathInfo = @"${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.info.log";
        //Describes path for "warn" file
        private const string _pathWarn = @"${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.warn.log";
        //Describes path for "error" file
        private const string _pathError = @"${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.error.log";
        LoggerOptions _loggerOptions;


        #endregion

        #region Field(s)
        /// <summary>
        /// Current dictionary contains messageType as key, and delegate Action<string> as a value 
        /// </summary>
        private Dictionary<MessageType, Action<string>> _funcMessage = new Dictionary<MessageType, Action<string>>();

        /// <summary>
        /// Current dictionary contains messageType as key, and delegate Action<Exception> as a value 
        /// </summary>
        private Dictionary<MessageType, Action<Exception>> _funcExc = new Dictionary<MessageType, Action<Exception>>();

        /// <summary>
        /// Current dictionary contains messageType as key, and delegate Action<Exception,string> as a value 
        /// </summary>
        private Dictionary<MessageType, Action<Exception, string>> _funcExcMessage = new Dictionary<MessageType, Action<Exception, string>>();

        /// <summary>
        /// Current dictionary contains messageType as key, and name of target as a value
        /// </summary>
        private Dictionary<MessageType, string> _targetNames = new Dictionary<MessageType, string>();

        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settingsService"> Provide parameters from config file </param>
        //public NLogger(LoggerOptions loggerOptions)
        public NLogger()
        {
            //_loggerOptions = loggerOptions;
            Init();
        }

        #endregion

        #region Public method

        /// <summary>
        /// Current method write only messages into files devided by specific message type.Default type is Info
        /// </summary>
        /// <param name="message">string parameter which will be written in file</param>
        /// <param name="type"> the type of message </param>
        public void Save(string message, MessageType type = MessageType.Info)
        {
            try { _funcMessage[type](message); } catch (Exception) { }
        }

        /// <summary>
        /// Current method write only errors into files devided by specific message type.Default type is Error
        /// </summary>
        /// <param name="ex">Exseption object contains information about exception</param>
        /// <param name="type">the type of message</param>
        public void Save(Exception ex, MessageType type = MessageType.Error)
        {
            try { _funcExc[type](ex); } catch (Exception) { }
        }

        /// <summary>
        /// Current method write exception  errors with specific messages. Default type is Error
        /// </summary>
        /// <param name="ex">Exception object contains information about exceptions</param>
        /// <param name="message">This field provide additional information which will be written into file</param>
        /// <param name="type">the type of message</param>
        public void Save(Exception ex, string message, MessageType type = MessageType.Error)
        {
            try { _funcExcMessage[type](ex, message); } catch (Exception) { }
        }
        /// <summary>
        /// Current method is used only in unit tests.
        /// </summary>
        /// <param name="type">Type of message </param>
        /// <returns></returns>
        public bool CheckFolder(MessageType type)
        {
            var fileTarget = (FileTarget)LogManager.Configuration.FindTargetByName(_targetNames[type]);
            var logEventInfo = new LogEventInfo { TimeStamp = DateTime.Now };
            string fileName = fileTarget.FileName.Render(logEventInfo);
            if (!File.Exists(fileName)) return false;
            return true;
        }

        #endregion

        #region Private method(s)

        /// <summary>
        /// Set settings for Nlog config
        /// </summary>
        private void Init()
        {
            _targetNames.Add(MessageType.Info, "info");
            _targetNames.Add(MessageType.Warn, "warn");
            _targetNames.Add(MessageType.Error, "error");
            //var layout = !string.IsNullOrEmpty(loggerOptions.layout) ? loggerOptions.layout : _layout;
            var layout = _layout;
            //settingsService.Get("logger:layout", _layout);

            //var pathInfo = settingsService.Get("logger:path:info", _pathInfo);
            //var pathWarn = settingsService.Get("logger:path:warn", _pathWarn);
            //var pathError = settingsService.Get("logger:path:error", _pathError);

            var pathInfo = _pathInfo;
            var pathWarn = _pathWarn;
            var pathError = _pathError;

            var config = new LoggingConfiguration();

            var consoleTarget = new ColoredConsoleTarget("console")
            {
                Layout = layout
            };

            var infoTarget = new FileTarget(_targetNames[MessageType.Info])
            {
                FileName = pathInfo,
                Layout = layout,
                AutoFlush = true,
                ConcurrentWrites = true
            };

            var warnTarget = new FileTarget(_targetNames[MessageType.Warn])
            {
                FileName = pathWarn,
                Layout = layout,
                AutoFlush = true,
                ConcurrentWrites = true
            };

            var errorTarget = new FileTarget(_targetNames[MessageType.Error])
            {
                FileName = pathError,
                Layout = layout,
                AutoFlush = true,
                ConcurrentWrites = true
            };

            config.AddTarget(consoleTarget);
            config.AddTarget(infoTarget);
            config.AddTarget(warnTarget);
            config.AddTarget(errorTarget);

            config.AddRuleForOneLevel(LogLevel.Info, infoTarget);
            config.AddRuleForOneLevel(LogLevel.Warn, warnTarget);
            config.AddRuleForOneLevel(LogLevel.Error, errorTarget);
            config.AddRuleForAllLevels(consoleTarget);

            LogManager.Configuration = config;

            init_func();
        }

        /// <summary>
        /// Set methods for delegate Action.  
        /// </summary>
        private void init_func()
        {
            //Get current class from NLog library
            var logger = LogManager.GetCurrentClassLogger();

            _funcMessage[MessageType.Info] = logger.Info;
            _funcMessage[MessageType.Warn] = logger.Warn;
            _funcMessage[MessageType.Error] = logger.Error;

            _funcExc[MessageType.Info] = logger.Info;
            _funcExc[MessageType.Warn] = logger.Warn;
            _funcExc[MessageType.Error] = logger.Error;

            _funcExcMessage[MessageType.Info] = logger.Info;
            _funcExcMessage[MessageType.Warn] = logger.Warn;
            _funcExcMessage[MessageType.Error] = logger.Error;
        }

        #endregion

        
    }
}
