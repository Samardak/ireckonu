﻿### NLOG.
NLOG uses to write messages in file.


To start using library you need inject ILog in constructor

public class ExampleService
{
	private ICache _iLog {get;set;}
	public ExampleService(ILog iLog)
	{
		_iLog = iLog;
	}

	public void SomeAction(object data)
	{
		_iLog.Info(data.ToString());
	}
}
Also you should configure your services in StartUp.cs file

public void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<ILog, Logger>();    // inject Logger as ILog
    }

    ### appSettings.json 
    Logger is required section:

	 "logger": {
    "layout": "${date:format=dd.MM.yyyy HH\\:mm\\:ss} ${level}: ${message} ${exception:format=toString,Data}",//Format messages
    "path": {
      "info": "${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.info.log",//Path to directory for info logs.
      "warn": "${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.warn.log",//Path to directory for warn logs.
      "error": "${basedir}/logs/${date:format=dd.MM.yyyy:cached=false}.error.log"//Path to directory for error logs.
    }
  },