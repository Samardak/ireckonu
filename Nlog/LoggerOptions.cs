﻿namespace NLogger
{
    public class LoggerOptions
    {
        public string layout { get; set; }
        public string pathInfo { get; set; }
        public string pathWarn { get; set; }
        public string pathError { get; set; }
    }
}
