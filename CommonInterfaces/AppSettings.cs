﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class AppSettings
    {
        /// <summary>
        /// path to folder we should save json file.
        /// </summary>
        public string fullpath { get; set; }
        public List<string> allowedFileExtensions { get; set; }
        /// <summary>
        /// how many size of file use can upload.
        /// </summary>
        public int MaxSizeFileMB { get; set; } = 5;
    }
}
