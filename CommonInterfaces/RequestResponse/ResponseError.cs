﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Enum;

namespace Common.RequestResponse
{
    public class ResponseError
    {
        /// <summary>
        /// contains ErrorEnum codes and description.
        /// </summary>
        static Dictionary<ResponseCodeEnum, string> _errors = new Dictionary<ResponseCodeEnum, string>();
        static ResponseError()
        {
            _errors[ResponseCodeEnum.PostedFileIsNull] = "UploadedFile can't be null. Please upload file to Form.Files.";
            _errors[ResponseCodeEnum.IncorrectFileExtension] = "Incorrect file extension. Please use only csv.";
            _errors[ResponseCodeEnum.ReachLimitSizeFile] = "Please upload file less then {0} MB";

            _errors[ResponseCodeEnum.ParsedDataIsNull] = "Parsed Data Is Null";
            _errors[ResponseCodeEnum.ParsedDataNoContainsElements] = "Parsed Data No Contains Elements";
            _errors[ResponseCodeEnum.DataStorageFailed] = "Data Storage Failed";
        }

        public string Message { get; set; }
        public ResponseCodeEnum Code { get; set; }

        public ResponseError(ResponseCodeEnum code, object param = null)
        {
            this.Code = code;
            _errors.TryGetValue(code, out var message);
            if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(param?.ToString()))
                message = string.Format(message, param);
            this.Message = message;
        }



    }
}
