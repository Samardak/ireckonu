﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    /// <summary>
    /// Data parser
    /// </summary>
    public interface IParser
    {
        /// <summary>
        /// Parse string and return list of objects
        /// </summary>
        /// <param name="data">csv string data</param>
        /// <returns>List of items</returns>
        List<IreckonuData> Parse(string data);
    }
}
