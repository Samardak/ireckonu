﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    /// <summary>
    /// Interface for data storage (db, file system, multiple.)
    /// </summary>
    public interface IDataStorage
    {
        Task<bool> Save(IEnumerable<IreckonuData> datas);
    }
}
