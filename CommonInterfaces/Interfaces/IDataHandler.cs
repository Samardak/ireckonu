﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;

namespace Common.Interfaces
{
    /// <summary>
    /// Interface for data handling.
    /// </summary>
    public interface IDataHandler
    {
        /// <summary>
        /// Here we can call IParser and IDataStorage to handle data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<ResponseCodeEnum> Handle(string data);
    }
}
