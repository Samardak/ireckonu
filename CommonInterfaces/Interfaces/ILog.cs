﻿using System;

namespace CommonInterfaces
{
    public enum MessageType
    {
        Info = 0,
        Warn = 1,
        Error = 2
    }

    public interface ILog
    {
        /// <summary>
        /// Current method write only messages into files devided by specific message type.Default type is Info
        /// </summary>
        /// <param name="message">string parameter which will be written in file</param>
        /// <param name="type"> the type of message </param>
        void Save(string message, MessageType type = MessageType.Error);

        /// <summary>
        /// Current method write only errors into files devided by specific message type. Default type is Error
        /// </summary>
        /// <param name="ex">Exception object contains information about exceptions</param>
        /// <param name="type">the type of message</param>
        void Save(Exception ex, MessageType type = MessageType.Error);

        /// <summary>
        /// Current method write exception  errors with specific messages. Default type is Error
        /// </summary>
        /// <param name="ex">Exception object contains information about exceptions</param>
        /// <param name="message">This field provide additional information which will be written into file</param>
        /// <param name="type">the type of message</param>
        void Save(Exception ex, string message, MessageType type = MessageType.Error);

        /// <summary>
        /// Current method use only for unit tests.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool CheckFolder(MessageType type);
    }
}
