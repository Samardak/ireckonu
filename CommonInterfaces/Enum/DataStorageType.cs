﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Enum
{
    public enum DataStorageType
    {
        Db = 1,              //save data only to db
        FileSystem = 2,      //save data only to file system
        Multiple = 3,        //save data to db and file system
    }
}
