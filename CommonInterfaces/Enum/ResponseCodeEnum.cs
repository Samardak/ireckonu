﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Enum
{
    public enum ResponseCodeEnum
    {
        PostedFileIsNull,
        IncorrectFileExtension,
        ReachLimitSizeFile,
        ParsedDataIsNull,
        ParsedDataNoContainsElements,
        DataStorageFailed,
        Ok
    }
}
