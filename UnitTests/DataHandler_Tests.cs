﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using Common.Enum;
using Common.Interfaces;
using CommonInterfaces;
using NSubstitute;
using Services;
using Shouldly;
using Xunit;

namespace UnitTests
{
    public class DataHandler_Tests
    {
        [Fact]
        public void DataHandler_Successful()
        {
            var iParser = Substitute.For<IParser>();
            var iDataStorage = Substitute.For<IDataStorage>();
            var str = "00000023900st,23,test,Mexx,1,0,1-3 werkdagen,boy,140,zwart";

            var mockData = new List<IreckonuData>()
            {
                new IreckonuData
                {
                    DeliveredIn = "1-3 werkdagen",
                    ArtikelCode = "23",
                    Color = "zwart",
                    DiscountPrice = 0,
                    Price = 1,
                    Key = "00000023900st",
                    Q1 = "boy",
                    Size = 140,
                    Description = "Mexx",
                    ColorCode = "test"
                }
            };
            iParser.Parse(Arg.Any<string>())
                .ReturnsForAnyArgs(mockData);

            iDataStorage.Save(Arg.Any<List<IreckonuData>>())
                .ReturnsForAnyArgs(true);

            IDataHandler dataHandler = new DataHandler(iParser, iDataStorage);
            var result = dataHandler.Handle(str).Result;
            result.ShouldBe(ResponseCodeEnum.Ok);
        }
    }
}
