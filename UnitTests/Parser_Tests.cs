﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonInterfaces;
using NSubstitute;
using Services;
using Shouldly;
using Xunit;

namespace UnitTests
{
    public class Parser_Tests
    {
        [Fact]
        public void Parse_Successful()
        {
            var iLog = Substitute.For<ILog>();
            DataParser dataParser = new DataParser(iLog);
            var strB = new StringBuilder();
            strB.AppendLine("Key,ArtikelCode,ColorCode,Description,Price,DiscountPrice,DeliveredIn,Q1,Size,Color");
            strB.AppendLine("00000002groe62,2,broek,Gaastra,8,0,1-3 werkdagen,baby,62,groen");

            var items = dataParser.Parse(strB.ToString());
            items.ShouldNotBeNull();
            items.Any().ShouldBe(true);
        }
    }
}
