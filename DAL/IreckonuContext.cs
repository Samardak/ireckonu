﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.models;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class IreckonuContext : DbContext
    {
        public DbSet<Artikel> Artikel { get; set; }
        public DbSet<Color> Color { get; set; }
        public DbSet<DeliveredIn> DeliveredIn { get; set; }
        public DbSet<Product> Product { get; set; }

        public IreckonuContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}
