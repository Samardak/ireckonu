﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.models
{
    public class Artikel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ArtikelId { get; set; }
        public string ArtikelCode { get; set; }
        public string Description { get; set; }
        public string Q1 { get; set; }
    }
}
