﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Text;

namespace DAL.models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ProductId { get; set; }
        public string Key { get; set; }
        public long ArtikelId { get; set; }
        public long Price { get; set; }
        public long DiscountPrice { get; set; }
        public long DeliveredInId { get; set; }
        public int Size { get; set; }
        public long ColorId { get; set; }

        public Color Color { get; set; }
        public DeliveredIn DeliveredIn { get; set; }
        public Artikel Artikel { get; set; }
    }
}
